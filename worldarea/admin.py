from django.contrib import admin

# Register your models here.

from .models import Province, City, Area, Street, Village


# 实现只读站点
class ReadOnlyAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_list_display(self, request):
        return [field.name for field in self.model._meta.concrete_fields]

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields] + \
               [field.name for field in obj._meta.many_to_many]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


# @admin.register(Province)
# class ContinentsAdmin(ReadOnlyAdmin):
#     search_fields = ('name', 'code',)
#
#
# @admin.register(City)
# class CountriesAdmin(ReadOnlyAdmin):
#     search_fields = ('name', 'code',)
#
#
# class AreaAdmin(ReadOnlyAdmin):
#     list_display = ('code', 'name', 'citycode', 'provincecode',)
#     autocomplete_fields = ['citycode', 'provincecode']
#
#
# admin.site.register(Area, AreaAdmin)
# admin.site.register(Street)
# admin.site.register(Village)

# class AdminClass(admin.ModelAdmin):
#     def __init__(self, model, admin_site):
#         # 列表页自动显示所有的字段
#         self.list_display = [field.name for field in model._meta.fields]
#         super(AdminClass, self).__init__(model, admin_site)
#
#
# from django.apps import apps
#
# models = apps.get_models()
#
# for model in models:
#     try:
#         admin.site.register(model,AdminClass)
#     except admin.sites.AlreadyRegistered:
#         pass
