# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from smart_selects.db_fields import ChainedForeignKey


class Province(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'province'
        verbose_name = '省'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name if self.name else ''


class City(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    provincecode = models.ForeignKey(Province, db_column='provinceCode', max_length=255, blank=True,
                                     null=True, on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'city'
        verbose_name = '城市'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name if self.name else ''


class Area(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    provincecode = models.ForeignKey(Province, db_column='provinceCode', max_length=255, blank=True,
                                     null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    citycode = models.ForeignKey(City, db_column='cityCode', max_length=255, blank=True,
                                 null=True, on_delete=models.SET_NULL)  # Field name made lowercase.

    # 省==>城市　链式关联键,django-smart-selects　not work
    # citycode = ChainedForeignKey(City, chained_field='provincecode', chained_model_field='provincecode',
    #                              show_all=False, auto_choose=True, sort=True,
    #                              db_column='citycode', max_length=255, blank=True,
    #                              null=True, on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'area'
        verbose_name = '地区'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name if self.name else ''


class Street(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    areacode = models.ForeignKey(Area, db_column='areaCode', max_length=255, blank=True,
                                 null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    provincecode = models.ForeignKey(Province, db_column='provinceCode', max_length=255, blank=True,
                                     null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    citycode = models.ForeignKey(City, db_column='cityCode', max_length=255, blank=True,
                                 null=True, on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'street'
        verbose_name = '街道'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name if self.name else ''


class Village(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    streetcode = models.ForeignKey(Street, db_column='streetCode', max_length=255, blank=True,
                                   null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    provincecode = models.ForeignKey(Province, db_column='provinceCode', max_length=255, blank=True,
                                     null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    citycode = models.ForeignKey(City, db_column='cityCode', max_length=255, blank=True,
                                 null=True, on_delete=models.SET_NULL)  # Field name made lowercase.
    areacode = models.ForeignKey(Area, db_column='areaCode', max_length=255, blank=True,
                                 null=True, on_delete=models.SET_NULL)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'village'
        verbose_name = '乡村'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name if self.name else ''
