from django.apps import AppConfig


class WorldareaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'worldarea'
