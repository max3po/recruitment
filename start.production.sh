#django-admin compilemessages
#export DJANGO_SETTINGS_MODULE=settings.production
python manage.py runserver 0.0.0.0:8000 $server_params

# for async web server:
# 注意：应用容器不支持js,css资源加载，不管prod还是local
#export DJANGO_SETTINGS_MODULE=settings.production
#uvicorn recruitment.asgi:application --port 8000 --workers 2