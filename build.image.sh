docker build -f Dockerfile-base -t smzh095/django-recruitment-base:1.1 .

## or just docker pull already built image from docker hub:
# docker pull smzh095/django-recruitment-base:1.1

## 构建包含 local.py 文件的 1.2 版本镜像（基于1.1镜像构建）：
docker build -f Dockerfile -t smzh095/django-recruitment:1.2 .

#sudo docker tag smzh095/django-recruitment:1.2 registry.cn-beijing.aliyuncs.com/django4study/smzh095:1.2
docker tag smzh095/django-recruitment:1.2 registry.cn-beijing.aliyuncs.com/django4study/smzh095:1.2
docker push registry.cn-beijing.aliyuncs.com/django4study/smzh095:1.2

## k8s 部署
## 部署前先替换 k8s/xxx-deployment.yaml 中的 {{BUILD_NUMBER}} 为版本号
## 然后运行 apply
## kubectl apply -f k8s
