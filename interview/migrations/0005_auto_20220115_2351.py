# Generated by Django 3.1 on 2022-01-15 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interview', '0004_alter_candidate_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='candidate',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
