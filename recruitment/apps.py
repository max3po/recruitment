from django.apps import apps, AppConfig
from django.contrib import admin


# class ListAdminMixin(object):
class AdminClass(admin.ModelAdmin):
    def __init__(self, model, admin_site):
        # 列表页自动显示所有的字段
        self.list_display = [field.name for field in model._meta.fields]
        # super(ListAdminMixin, self).__init__(model, admin_site)
        super(AdminClass, self).__init__(model, admin_site)


# automatically register all models
class UniversalManagerApp(AppConfig):
    """
    应用配置在　所有应用 Admin　都加载之后执行
    """
    # default_auto_field = 'django.db.models.BigAutoField'
    # verbose_name = 'Universal Model Managers'
    name = 'recruitment'

    def ready(self):
        # ready方法是在Django Admin启动server后，所有其他应用加载完后，recruitment应用才算加载完
        models = apps.get_app_config('worldarea').get_models()

        for model in models:
            # 动态的admin
            # admin_class = type('AdminClass', (ListAdminMixin, admin.ModelAdmin), {})
            try:
                admin.site.register(model, AdminClass)
            except admin.site.AlreadRegistered:
                pass
