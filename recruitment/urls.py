"""recruitment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import User
from jobs.models import Job
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
from . import views


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class JobSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'


# 将creator值由JobSerializer中Hyperlink类型("http://127.0.0.1:8000/api/users/1/")转换为对应用户名admin
class JobSerializer2(serializers.ModelSerializer):
    creator = serializers.SlugRelatedField(read_only=True, slug_field='username')

    class Meta:
        model = Job
        fields = ("url", "job_type", "job_name", "job_city", "job_responsibility", "job_requirement", "created_date",
                  "modified_date", "creator")
        url = serializers.HyperlinkedRelatedField(read_only=True, view_name='objects-detail')


class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer2


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'jobs', JobViewSet)

urlpatterns = [
    path("", include(('jobs.urls',"jobs"), namespace='jobs')),
    # django2.0中include()变为包含一个元祖,命名空间的作用：避免不同应用中的路由使用了相同的名字发生冲突
    # url('^', include('jobs.urls')),
    path('admin/', admin.site.urls),
    url(r'^admin/login/?$', views.login_with_captcha, name='adminlogin'),
    path('grappelli/', include('grappelli.urls')),  # grappelli URLS
    path('i18n/', include('django.conf.urls.i18n')),  # 多语言路径支持
    path('accounts/', include('registration.backends.simple.urls')),
    path('api-auth/', include('rest_framework.urls')),  # RESTFramework login&logout views
    path('api/', include(router.urls)),  # 具体的rest Api访问根路径
    # url(r'^chaining/', include('smart_selects.urls')),  # 省市链式下拉关联选择
    path('', include('django_prometheus.urls')),  # django_prometheus
    path('captcha/', include('captcha.urls')),  # 图形验证码
    path('clogin/', views.login_with_captcha, name='clogin'),  # 图形验证码

]

from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
# enable debug_toolbar in url mapping
# import debug_toolbar

# if settings.DEBUG:
#     urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] + urlpatterns

admin.site.site_header = _("匠果科技开放职位")
