from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from jobs.views import joblist, detail, ResumeCreateView, ResumeDetailView, detail_resume, create_hr_user
from django.urls import path
from django.conf import settings


def trigger_error(request):
    division_by_zero = 1 / 0


urlpatterns = [
    # 127.0.0.1:8000首页自动跳转　到职位列表页
    url(r'^$', joblist, name='index'),
    # 职位列表页
    url(r'^joblist/$', joblist, name='joblist'),  # 使用name参数指明路由的名字
    # 职位详情页
    url(r'^job/(?P<job_id>\d+)/$', detail, name='detail'),
    # 提交职位简历
    path(r'resume/add/', ResumeCreateView.as_view(), name='resume-add'),
    # 简历详情页
    path(r'resume/<int:pk>/', ResumeDetailView.as_view(), name='resume-detail'),
    # sentry测试
    path('sentry-debug/', trigger_error),
    # 管理员创建hr账号的页面，测试csrf攻击
    path('create_hr_user/', create_hr_user,name='create_hr_user'),

]

# 演示xss攻击路由
if settings.DEBUG:
    urlpatterns += [url(r'detail_resume/(?P<resume_id>\d+)/$', detail_resume, name='detail-resume'),]
