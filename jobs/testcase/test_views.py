"""
Django 自带的代码(框架中实现的)逻辑不需要测试
自己写的代码需要测试,比如自定义的页面的访问,自定义的功能菜单
"""
from django.test import TestCase
from django.test import Client

from jobs.models import Job, JobCity, JobTypes


class JobTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.job = Job.objects.create(job_name="Java开发工程狮", job_type=JobTypes[0][0], job_city=JobCity[1][0],
                                     job_requirement="精通Java开发")

    def test1(self):
        # Some test using self.job
        pass

    def test_index(self):
        client = Client()
        response = client.get('/joblist/')
        # response = client.get('/job1234/')  # 404
        self.assertEqual(response.status_code, 200)

    def test_detail(self):
        # 使用TestCase.self.client作为HTTP Client:
        response = self.client.get('/job/1/')
        self.assertEqual(response.status_code, 200)

        job = response.context['job']
        self.assertEqual(job.job_name, JobTests.job.job_name)
