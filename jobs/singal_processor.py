from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Job, Resume
from interview.dingtalk import send

import json, logging

logger = logging.getLogger(__name__)


# 方式２：使用decorator来注册信号处理器;简历或工作修改保存后，变动记录
@receiver(signal=post_save, sender=Resume, dispatch_uid="resume_post_save_dispatcher")
@receiver(signal=post_save, sender=Job, dispatch_uid="job_post_save_dispatcher")
def post_save_callback(sender, instance=None, created=False, **kwarg):
    message = ""
    if isinstance(instance, Job):
        message = "Job for %s has been saved by %s" % (instance.job_name, instance.creator)
    else:
        message = "Resume for %s %s has been saved by %s" % (
        instance.apply_position, instance.username, instance.applicant)

    logger.info(message)
    send(message)


from django.forms.models import model_to_dict


def post_delete_callback(sender, instance=None, using=None, **kwargs):
    dict_obj = model_to_dict(instance, exclude=("picture", "attachment", "created_date", "modified_date"))
    message = "Instance of %s has been deleted: %s" % (
        type(instance),
        json.dumps(dict_obj, ensure_ascii=False))  # json_dumps(dict)时，如果dict包含有汉字，一定加上ensure_ascii=False
    logger.info(message)
    send(message)


# 方式１：手工信号处理器;简历删除后，变动记录
post_delete.connect(post_delete_callback, sender=Resume, dispatch_uid="resume_post_delete_dispatcher")
