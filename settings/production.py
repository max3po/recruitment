# 生产环境配置：敏感信息不提交到代码库
import os
from .base import *

# 从环境变量获取 SECRET_KEY，及其它敏感数据；
# 容器化的环境下，这些数据保存在专用的 secret 存储，或者 KMS 系统中
# 如 Kubernetes Secret中，云厂商的 KMS 服务，或者开源的 Vault 服务中

ALLOWED_HOSTS = ["127.0.0.1", "host.docker.internal", "*"]  # 生产环境会通过Nginx网关代理

SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY', "e=_*p535spqly5nc=)bzehx_tqcf)1(w%a-za6b55xnx-3yofh")
DEBUG = False

INSTALLED_APPS += (
    # 'debug_toolbar', # and other apps for local development
)

INTERNAL_IPS = [
    # ...
    '127.0.0.1',
    # ...
]

CELERY_BROKER_URL = 'redis://redis:6379/9'
CELERY_RESULT_BACKEND = 'redis://redis:6379/8'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Shanghai'
CELERYD_MAX_TASKS_PER_CHILD = 10
CELERYD_LOG_FILE = os.path.join(BASE_DIR, "logs", "celery_work.log")
CELERYBEAT_LOG_FILE = os.path.join(BASE_DIR, "logs", "celery_beat.log")

MIDDLEWARE += [
    # "debug_toolbar.middleware.DebugToolbarMiddleware",
]

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="http://46d84d8cea1f4f1c8b112fcf2ef4b787@121.5.17.66:9000/2",
    integrations=[DjangoIntegration()],
    # performance tracing sample rate, 采样率, 生产环境访问量过大时，建议调小（不用每一个URL请求都记录性能）
    traces_sample_rate=1.0,  # 日志的采样率-100%，性能采样率
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True  # 是不是发送个人标识的信息
)

# 如果仅使用数据库中的账号，以下 LDAP 配置可忽略
# 替换这里的配置为正确的域服务器配置，同时可能需要修改 base.py 中的 LDAP 服务器相关配置:
LDAP_AUTH_URL = os.environ.get('LDAP_AUTH_URL', 'ldap://localhost:389')
LDAP_AUTH_CONNECTION_USERNAME = os.environ.get('LDAP_AUTH_CONNECTION_USERNAME')
LDAP_AUTH_CONNECTION_PASSWORD = os.environ.get('LDAP_AUTH_CONNECTION_PASSWORD')  # openldap容器创建时指定

# TODO CDN加速暂未实现,阿里云申请域名未ICP备案
# STATIC_URL = 'http://icdn.ihopeit.com/static/'

# 将本机tengine配置/home/python/Downloads/tengine/conf/nginx.conf中指定静态资源的static路径
STATIC_URL = '/static/'

# 阿里云 CDN 存储静态资源文件 & 阿里云存储上传的图片/文件
STATICFILES_STORAGE = 'django_oss_storage.backends.OssStaticStorage'

# AliCloud access key ID
OSS_ACCESS_KEY_ID = os.environ.get('OSS_ACCESS_KEY_ID', 'LTAI5tED5Comv5U1FrejPW7t')
# AliCloud access key secret
OSS_ACCESS_KEY_SECRET = os.environ.get('OSS_ACCESS_KEY_SECRET', 'Id6NQ5t1FSsqYmXmxTNer2Swb4wu40')
# The name of the bucket to store files in
OSS_BUCKET_NAME = 'recruitment-django'
# OSS_BUCKET_NAME = 'recruit-static'

# The URL of AliCloud OSS endpoint
# Refer https://www.alibabacloud.com/help/zh/doc-detail/31837.htm for OSS Region & Endpoint
OSS_ENDPOINT = 'oss-cn-beijing.aliyuncs.com'

# 钉钉群机器人的 WEB_HOOK， 用于发送钉钉消息
DINGTALK_WEB_HOOK_TOKEN = os.environ.get('DINGTALK_WEB_HOOK_TOKEN', 'fa866494e15eecf7e214c14160633ec4e4b4801a49ded5289791eaa3f74b88e5')
DINGTALK_WEB_HOOK = "https://oapi.dingtalk.com/robot/send?access_token=%s" % DINGTALK_WEB_HOOK_TOKEN
