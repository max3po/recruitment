# 本地环境配置
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *

DEBUG = True

ALLOWED_HOSTS = ['recruit.ihopeit.com', "127.0.0.1",'*']

# INTERNAL_IPS = ["127.0.0.1"]  # Debug Toolbar for local development.
# 务必修改以下值，确保运行时系统安全:
SECRET_KEY = "e=_*p535spqly5nc=)bzehx_tqcf)1(w%a-za6b55xnx-3yofh"

# STATIC_URL = 'http://icdn.ihopeit.com/static/'
STATIC_URL = '/static/'
# 如果仅使用数据库中的账号，以下 LDAP 配置可忽略
# 替换这里的配置为正确的域服务器配置，同时可能需要修改 base.py 中的 LDAP 服务器相关配置:
LDAP_AUTH_URL = "ldap://localhost:389"
LDAP_AUTH_CONNECTION_USERNAME = "admin"
LDAP_AUTH_CONNECTION_PASSWORD = "admin_passwd_4_ldap"  # openldap容器创建时指定

INSTALLED_APPS += (
    # other apps for production site
    # "debug_toolbar",
)

MIDDLEWARE += [
    # "debug_toolbar.middleware.DebugToolbarMiddleware",
]

# 钉钉群机器人的 WEB_HOOK， 用于发送钉钉消息
DINGTALK_WEB_HOOK = "https://oapi.dingtalk.com/robot/send?access_token=fa866494e15eecf7e214c14160633ec4e4b4801a49ded5289791eaa3f74b88e5"

sentry_sdk.init(
    dsn="http://46d84d8cea1f4f1c8b112fcf2ef4b787@121.5.17.66:9000/2",
    integrations=[DjangoIntegration()],
    # performance tracing sample rate, 采样率, 生产环境访问量过大时，建议调小（不用每一个URL请求都记录性能）
    traces_sample_rate=1.0,  # 日志的采样率-100%，性能采样率
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True  # 是不是发送个人标识的信息
)

# CELERY_BROKER_URL = 'redis://localhost:6379/9'
CELERY_BROKER_URL = 'redis://redis:6379/9'  # 容器中无法识别具体ip，需要与本地127.0.0.1绑定
# CELERY_RESULT_BACKEND = 'redis://localhost:6379/8'
CELERY_RESULT_BACKEND = 'redis://redis:6379/8'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Shanghai'
CELERYD_MAX_TASKS_PER_CHILD = 10
CELERYD_LOG_FILE = os.path.join(BASE_DIR, "logs", "celery_work.log")
CELERYBEAT_LOG_FILE = os.path.join(BASE_DIR, "logs", "celery_beat.log")

# 启动本地 Celery 异步任务服务 & Flower 监控服务
# $ DJANGO_SETTINGS_MODULE=settings.local celery -A recruitment worker -l info
# * $ DJANGO_SETTINGS_MODULE=settings.local celery -A recruitment worker -l info
#
# $ DJANGO_SETTINGS_MODULE=settings.local celery -A recruitment flower
# * $ DJANGO_SETTINGS_MODULE=settings.local celery -A recruitment flower
#
# celery 从  4.x 升级到 5.x
# $ celery upgrade settings path/to/settings.py
# * $ celery upgrade settings path/to/settings.py

# 阿里云 CDN 存储静态资源文件 & 阿里云存储上传的图片/文件
DEFAULT_FILE_STORAGE = 'django_oss_storage.backends.OssMediaStorage'

# AliCloud access key ID
# OSS_ACCESS_KEY_ID = os.environ.get('OSS_ACCESS_KEY_ID','')
OSS_ACCESS_KEY_ID = 'LTAI5tED5Comv5U1FrejPW7t'
# AliCloud access key secret
# OSS_ACCESS_KEY_SECRET = os.environ.get('OSS_ACCESS_KEY_SECRET','')
OSS_ACCESS_KEY_SECRET = 'Id6NQ5t1FSsqYmXmxTNer2Swb4wu40'
# The name of the bucket to store files in
OSS_BUCKET_NAME = 'recruitment-django'

# The URL of AliCloud OSS endpoint
# Refer https://www.alibabacloud.com/help/zh/doc-detail/31837.htm for OSS Region & Endpoint
OSS_ENDPOINT = 'oss-cn-beijing.aliyuncs.com'