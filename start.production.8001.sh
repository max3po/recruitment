django-admin compilemessages
#python manage.py runserver 0.0.0.0:8000 --settings=settings.production

# for async web server:
# 注意：应用容器不支持js,css资源加载，不管prod还是local
export DJANGO_SETTINGS_MODULE=settings.production
uvicorn recruitment.asgi:application --port 8001 --workers 2
#uvicorn recruitment.asgi:application --lifespan on